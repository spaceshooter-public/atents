﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour
{
    public int WeaponPower = 5; // 플레이어 무기 공격력
    public float WeponDistance = 50f; // 플레이어 무기 사정거리
    public int hp = 20; // 플레이어 체력 변수
    public GameObject EnvirEffect; // 일반 피격 이펙트 오브젝트
    public GameObject EnemyEffect; // 에너미 피격 이펙트 오브젝트
    public GameObject RocksEffect; // 돌 피격 이펙트 오브젝트
    //public Transform firepos;
    //public GameObject shotEffect;

    void Update()
    {
        Controlray();
    }

    void Controlray()
    {
        //레이캐스트 궤적 확인용 Debug.DrawRay
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * WeponDistance, Color.red);

        if (Input.GetMouseButtonDown(0))
        {
            // 레이를 생성하고 발사될 위치와 진행 방향을 설정한다.
            Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);

            // 레이가 부딪힌 대상의 정보를 저장할 변수를 생성한다.
            RaycastHit hitInfo;

            Vector3 hitposition = Vector3.zero;

            // 레이의 제한 길이는 20, 레이를 발사하고, 만일 부딪힌 물체가 있으면.
            if (Physics.Raycast(ray, out hitInfo, WeponDistance))
            {
                // 레이가 부딪힌 지점으로 카메라를 이동시킨다.
                Camera.main.transform.position = hitInfo.point;

                // 피격 이펙트의 forward 방향을 레이가 부딪힌 지점의 법선 벡터와 일치시킨다.
                Camera.main.transform.forward = hitInfo.normal;

                hitposition = hitInfo.point;

                // 만일 레이에 부딪힌 대상의 레이어가 "Rocks"라면 이펙트를 실행한다.
                if (hitInfo.transform.gameObject.layer == LayerMask.NameToLayer("Rocks"))
                {
                    StartCoroutine(rocksEffect(hitposition));
                }

                // 만일 레이에 부딪힌 대상의 레이어가 "Rocks"가 아니고 "Envir"라면 이펙트를 실행한다.
                else if (hitInfo.transform.gameObject.layer == LayerMask.NameToLayer("Envir"))
                {
                    StartCoroutine(envirEffect(hitposition));
                }

                // 에너미에서는 메모리 관리에 장점이 있는 CompareTag를 사용하였다.
                // 에너미에는 캡슐콜라이더를 적용하였다.
                // 만일 레이에 부딪힌 대상의 태그가 "ENEMY"라면 이펙트를 실행한다.
                else if (hitInfo.collider.CompareTag("ENEMY"))
                {
                    //MonsterContrl 스크립트를 참조하기 위해 컴포넌트를 연결한다.
                    MonsterControl mctrl = hitInfo.transform.GetComponent<MonsterControl>();

                    //Damaged 에니메이션을 호출한다.
                    mctrl.anim.SetTrigger("Damaged");
                    
                    //동적으로 이펙트를 생성한다.
                    Instantiate(EnemyEffect, hitInfo.point, hitInfo.transform.rotation);
                }
            }
        }
    }

    //반복문 waitfor변수 설정
    WaitForSeconds waitfor = new WaitForSeconds(0.03f);

    private IEnumerator rocksEffect(Vector3 hitposition)
    {
        //Instantiate(shotEffect, firepos.position, firepos.rotation);
        //동적으로 이펙트를 생성한다.
        Instantiate(RocksEffect, hitposition, Quaternion.identity);
        // waitfor만큼 잠시 처리를 대기
        yield return waitfor;
    }

    private IEnumerator envirEffect(Vector3 hitposition)
    {
        //동적으로 이펙트를 생성한다.
        Instantiate(EnvirEffect, hitposition, Quaternion.identity);
        // waitfor만큼 잠시 처리를 대기 
        yield return waitfor;
    }
}