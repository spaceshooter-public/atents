using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFireBomb : MonoBehaviour
{
    public GameObject BombPosition; // 수류탄 발사 위치
    public GameObject BombPrefab; // 수류탄 프리팹
    public float BombThrowpower = 15f; // 수류탄 투척 파워

    void Update()
    {
        Bomb();
    }

    void Bomb()
    {
        // 마우스 오른쪽 버튼 입력을 받는다.
        if (Input.GetMouseButtonDown(1))
        {
            // 수류탄 오브젝트를 생성하고, 수류탄의 생성 위치를 발사 위치로 한다.
            GameObject bomb = Instantiate(BombPrefab);
            bomb.transform.position = BombPosition.transform.position;

            // 수류탄 오브젝트의 Rigidbody 컴포넌트를 가져온다.
            Rigidbody rb = bomb.GetComponent<Rigidbody>();

            // 카메라의 정면 방향으로 수류탄에 물리적인 힘을 가한다.
            rb.AddForce(Camera.main.transform.forward * BombThrowpower, ForceMode.Impulse);
        }
    }
}
