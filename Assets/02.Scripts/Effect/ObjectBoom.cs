using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBoom : MonoBehaviour
{
    //폴발 효과 파티클을 연결할 변수
    public GameObject ObjectBoomEffect;

    //컴포넌트를 저장할 변수
    private Transform Tr;
    private Rigidbody rb;

    //총알 맞은 횟수를 누적시킬 변수
    private int hitCount = 0;

    void Start()
    {
        Tr = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
    }

    //충돌 시 발생하는 콜백 함수
    void OnCollisionEnter(Collision coll)
    {
        if (coll.collider.CompareTag("BULLET"))
        {
            //총알 맞은 횟수를 증가시키고 3회 이상이면 폭발 처리
            if (++hitCount == 3)
            {
                ExpObject();
            }
        }
    }

    void ExpObject()
    {
        //폭발 효과 파티클 생성
        GameObject exp = Instantiate(ObjectBoomEffect, Tr.position, Quaternion.identity);

        //Rigidbody 컴포넌트의 mass를 1.0으로 수정해 무게를 가볍게 함
        rb.mass = 1.0f;
        //위로 솟구치는 힘을 가함
        rb.AddForce(Vector3.up * 1500.0f);

        //3초 후에 오브젝트 제거
        Destroy(gameObject, 3.0f);
    }
}
