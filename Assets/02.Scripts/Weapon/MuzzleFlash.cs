using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleFlash : MonoBehaviour
{
    public Transform Firepos;
    public ParticleSystem ShotEffect;

    void Update()
    {
        Shot();
    }

    void Shot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ShotEffect.Play();
            //동적으로 이펙트를 생성한다.
            //Instantiate(ShotEffect, Firepos.position, Firepos.rotation);
            //StartCoroutine(shotEffect());
        }
    }

    //반복문 waitfor변수 설정
    /*WaitForSeconds waitfor = new WaitForSeconds(0.0f);

    IEnumerator shotEffect()
    {
        //동적으로 이펙트를 생성한다.
        Instantiate(ShotEffect, Firepos.position, Firepos.rotation);
        yield return waitfor;
    }*/
}
